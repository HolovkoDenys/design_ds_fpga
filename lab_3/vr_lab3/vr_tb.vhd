-- Vhdl test bench created from schematic D:\XI_LABS\vr_lab3\vr_sh.sch - Tue Dec 19 23:05:01 2017
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY vr_sh_vr_sh_sch_tb IS
END vr_sh_vr_sh_sch_tb;
ARCHITECTURE behavioral OF vr_sh_vr_sh_sch_tb IS 

   COMPONENT vr_sh
   PORT( vr_a	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          XLXN_3	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0); 
          vr_c_ip	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0));
   END COMPONENT;

   SIGNAL vr_a	:	STD_LOGIC_VECTOR (15 DOWNTO 0):=x"0001";
   SIGNAL XLXN_3	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
   SIGNAL vr_c_ip	:	STD_LOGIC_VECTOR (31 DOWNTO 0);

BEGIN

   UUT: vr_sh PORT MAP(
		vr_a => vr_a, 
		XLXN_3 => XLXN_3, 
		vr_c_ip => vr_c_ip
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
		vr_a <= x"1234" after 10 ns;
      WAIT for 10 ns; -- will wait forever
		vr_a <= x"FFFF" after 10 ns;
		wait for 10 ns;
		vr_a <= x"0000" after 10 ns;
		wait for 10 ns;
		vr_a <= x"4EC9" after 10 ns;
		wait for 10 ns;
	END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
