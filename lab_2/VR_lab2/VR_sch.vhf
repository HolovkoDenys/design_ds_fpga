--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : VR_sch.vhf
-- /___/   /\     Timestamp : 11/30/2017 12:56:12
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -sympath D:/XI_LABS/VR_lab2/ipcore_dir -intstyle ise -family artix7 -flat -suppress -vhdl D:/XI_LABS/VR_lab2/VR_sch.vhf -w D:/XI_LABS/VR_lab2/VR_sch.sch
--Design Name: VR_sch
--Device: artix7
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity VR_sch is
   port ( VR_a    : in    std_logic_vector (7 downto 0); 
          VR_b    : in    std_logic_vector (7 downto 0); 
          VR_c_ip : out   std_logic_vector (15 downto 0); 
          VR_c_m  : out   std_logic_vector (15 downto 0); 
          VR_c_mm : out   std_logic_vector (15 downto 0));
end VR_sch;

architecture BEHAVIORAL of VR_sch is
   component VR_mm
      port ( VR_a : in    std_logic_vector (7 downto 0); 
             VR_b : in    std_logic_vector (7 downto 0); 
             VR_c : out   std_logic_vector (15 downto 0));
   end component;
   
   component VR_mult
      port ( VR_a : in    std_logic_vector (7 downto 0); 
             VR_b : in    std_logic_vector (7 downto 0); 
             VR_c : out   std_logic_vector (15 downto 0));
   end component;
   
   component VR_ip_m
      port ( a : in    std_logic_vector (7 downto 0); 
             b : in    std_logic_vector (7 downto 0); 
             p : out   std_logic_vector (15 downto 0));
   end component;
   
begin
   XLXI_1 : VR_mm
      port map (VR_a(7 downto 0)=>VR_a(7 downto 0),
                VR_b(7 downto 0)=>VR_b(7 downto 0),
                VR_c(15 downto 0)=>VR_c_mm(15 downto 0));
   
   XLXI_2 : VR_mult
      port map (VR_a(7 downto 0)=>VR_a(7 downto 0),
                VR_b(7 downto 0)=>VR_b(7 downto 0),
                VR_c(15 downto 0)=>VR_c_m(15 downto 0));
   
   XLXI_3 : VR_ip_m
      port map (a(7 downto 0)=>VR_a(7 downto 0),
                b(7 downto 0)=>VR_b(7 downto 0),
                p(15 downto 0)=>VR_c_ip(15 downto 0));
   
end BEHAVIORAL;


