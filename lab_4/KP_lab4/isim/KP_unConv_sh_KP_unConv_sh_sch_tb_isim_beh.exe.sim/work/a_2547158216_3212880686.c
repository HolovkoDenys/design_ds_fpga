/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/XI_LABS/KP_lab4/KP_unConv_tb.vhd";



static void work_a_2547158216_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    int64 t3;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;

LAB0:    t1 = (t0 + 3312U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(60, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6078);
    t5 = (t0 + 3696);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3696);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(61, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6094);
    t5 = (t0 + 3760);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3760);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(62, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6110);
    t5 = (t0 + 3824);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3824);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(63, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6126);
    t5 = (t0 + 3888);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3888);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(64, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 3120);
    xsi_process_wait(t2, t3);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(65, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6142);
    t5 = (t0 + 3696);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3696);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(66, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6158);
    t5 = (t0 + 3760);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3760);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(67, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6174);
    t5 = (t0 + 3824);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3824);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(68, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6190);
    t5 = (t0 + 3888);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3888);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(69, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 3120);
    xsi_process_wait(t2, t3);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    xsi_set_current_line(70, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6206);
    t5 = (t0 + 3696);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3696);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(71, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6222);
    t5 = (t0 + 3760);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3760);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(72, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6238);
    t5 = (t0 + 3824);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3824);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(73, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6254);
    t5 = (t0 + 3888);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3888);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(74, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 3120);
    xsi_process_wait(t2, t3);

LAB14:    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

LAB12:    xsi_set_current_line(75, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6270);
    t5 = (t0 + 3696);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3696);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(76, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6286);
    t5 = (t0 + 3760);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3760);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(77, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6302);
    t5 = (t0 + 3824);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3824);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(78, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 6318);
    t5 = (t0 + 3888);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_delta(t5, 0U, 16U, t3);
    t10 = (t0 + 3888);
    xsi_driver_intertial_reject(t10, t3, t3);
    xsi_set_current_line(79, ng0);
    t3 = (10 * 1000LL);
    t2 = (t0 + 3120);
    xsi_process_wait(t2, t3);

LAB18:    *((char **)t1) = &&LAB19;
    goto LAB1;

LAB13:    goto LAB12;

LAB15:    goto LAB13;

LAB16:    goto LAB2;

LAB17:    goto LAB16;

LAB19:    goto LAB17;

}


extern void work_a_2547158216_3212880686_init()
{
	static char *pe[] = {(void *)work_a_2547158216_3212880686_p_0};
	xsi_register_didat("work_a_2547158216_3212880686", "isim/KP_unConv_sh_KP_unConv_sh_sch_tb_isim_beh.exe.sim/work/a_2547158216_3212880686.didat");
	xsi_register_executes(pe);
}
