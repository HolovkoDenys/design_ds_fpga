<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_2" />
        <signal name="ab(31:0)" />
        <signal name="d2(31:0)" />
        <signal name="KP_A(15:0)" />
        <signal name="KP_B(15:0)" />
        <signal name="KP_D(15:0)" />
        <signal name="ab(15:0)" />
        <signal name="d2(15:0)" />
        <signal name="d2(31:16)" />
        <signal name="ab(31:16)" />
        <signal name="KP_CO" />
        <signal name="XLXN_19" />
        <signal name="sum_less(15:0)" />
        <signal name="sum_great(31:16)" />
        <signal name="KP_T(15:0)" />
        <signal name="t(31:16)" />
        <signal name="KP_Less_R(15:0)" />
        <signal name="KP_Great_R(15:0)" />
        <signal name="XLXN_35" />
        <signal name="KP_CO_Res" />
        <port polarity="Input" name="KP_A(15:0)" />
        <port polarity="Input" name="KP_B(15:0)" />
        <port polarity="Input" name="KP_D(15:0)" />
        <port polarity="Output" name="KP_CO" />
        <port polarity="Input" name="KP_T(15:0)" />
        <port polarity="Output" name="KP_Less_R(15:0)" />
        <port polarity="Output" name="KP_Great_R(15:0)" />
        <port polarity="Output" name="KP_CO_Res" />
        <blockdef name="kp_mult">
            <timestamp>2017-12-11T14:0:50</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="adsu16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="48" y1="-64" y2="-64" x1="128" />
            <line x2="128" y1="-96" y2="-64" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="constant">
            <timestamp>2006-1-1T10:10:10</timestamp>
            <rect width="112" x="0" y="0" height="64" />
            <line x2="112" y1="32" y2="32" x1="144" />
        </blockdef>
        <block symbolname="kp_mult" name="XLXI_1">
            <blockpin signalname="KP_A(15:0)" name="kp_a(15:0)" />
            <blockpin signalname="KP_B(15:0)" name="kp_b(15:0)" />
            <blockpin signalname="ab(31:0)" name="kp_c(31:0)" />
        </block>
        <block symbolname="kp_mult" name="XLXI_2">
            <blockpin signalname="KP_D(15:0)" name="kp_a(15:0)" />
            <blockpin signalname="KP_D(15:0)" name="kp_b(15:0)" />
            <blockpin signalname="d2(31:0)" name="kp_c(31:0)" />
        </block>
        <block symbolname="add16" name="XLXI_4">
            <blockpin signalname="ab(15:0)" name="A(15:0)" />
            <blockpin signalname="d2(15:0)" name="B(15:0)" />
            <blockpin name="CI" />
            <blockpin signalname="XLXN_2" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="sum_less(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_5">
            <blockpin signalname="ab(31:16)" name="A(15:0)" />
            <blockpin signalname="d2(31:16)" name="B(15:0)" />
            <blockpin signalname="XLXN_2" name="CI" />
            <blockpin signalname="KP_CO" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="sum_great(31:16)" name="S(15:0)" />
        </block>
        <block symbolname="adsu16" name="XLXI_6">
            <blockpin signalname="sum_less(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_35" name="ADD" />
            <blockpin signalname="KP_T(15:0)" name="B(15:0)" />
            <blockpin name="CI" />
            <blockpin signalname="XLXN_19" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="KP_Less_R(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="adsu16" name="XLXI_7">
            <blockpin signalname="sum_great(31:16)" name="A(15:0)" />
            <blockpin signalname="XLXN_35" name="ADD" />
            <blockpin signalname="t(31:16)" name="B(15:0)" />
            <blockpin signalname="XLXN_19" name="CI" />
            <blockpin signalname="KP_CO_Res" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="KP_Great_R(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="constant" name="XLXI_8">
            <attr value="0" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="t(31:16)" name="O" />
        </block>
        <block symbolname="constant" name="XLXI_10">
            <attr value="0" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_35" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="688" y="352" name="XLXI_1" orien="R0">
        </instance>
        <instance x="688" y="576" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1504" y="576" name="XLXI_4" orien="R0" />
        <instance x="1504" y="1056" name="XLXI_5" orien="R0" />
        <branch name="XLXN_2">
            <wire x2="1504" y1="560" y2="608" x1="1504" />
            <wire x2="1952" y1="560" y2="560" x1="1504" />
            <wire x2="1952" y1="512" y2="560" x1="1952" />
        </branch>
        <branch name="ab(31:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1168" y="256" type="branch" />
            <wire x2="1168" y1="256" y2="256" x1="1072" />
            <wire x2="1248" y1="256" y2="256" x1="1168" />
        </branch>
        <branch name="d2(31:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1152" y="480" type="branch" />
            <wire x2="1152" y1="480" y2="480" x1="1072" />
            <wire x2="1264" y1="480" y2="480" x1="1152" />
        </branch>
        <branch name="KP_A(15:0)">
            <wire x2="688" y1="256" y2="256" x1="464" />
        </branch>
        <branch name="KP_B(15:0)">
            <wire x2="688" y1="320" y2="320" x1="464" />
        </branch>
        <branch name="KP_D(15:0)">
            <wire x2="592" y1="480" y2="480" x1="464" />
            <wire x2="688" y1="480" y2="480" x1="592" />
            <wire x2="592" y1="480" y2="544" x1="592" />
            <wire x2="688" y1="544" y2="544" x1="592" />
        </branch>
        <iomarker fontsize="28" x="464" y="256" name="KP_A(15:0)" orien="R180" />
        <iomarker fontsize="28" x="464" y="320" name="KP_B(15:0)" orien="R180" />
        <iomarker fontsize="28" x="464" y="480" name="KP_D(15:0)" orien="R180" />
        <branch name="ab(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1440" y="256" type="branch" />
            <wire x2="1440" y1="256" y2="256" x1="1376" />
            <wire x2="1504" y1="256" y2="256" x1="1440" />
        </branch>
        <branch name="d2(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1440" y="384" type="branch" />
            <wire x2="1440" y1="384" y2="384" x1="1376" />
            <wire x2="1504" y1="384" y2="384" x1="1440" />
        </branch>
        <branch name="d2(31:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1408" y="864" type="branch" />
            <wire x2="1408" y1="864" y2="864" x1="1376" />
            <wire x2="1504" y1="864" y2="864" x1="1408" />
        </branch>
        <branch name="ab(31:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1424" y="736" type="branch" />
            <wire x2="1424" y1="736" y2="736" x1="1376" />
            <wire x2="1504" y1="736" y2="736" x1="1424" />
        </branch>
        <branch name="KP_CO">
            <wire x2="2032" y1="992" y2="992" x1="1952" />
        </branch>
        <iomarker fontsize="28" x="2032" y="992" name="KP_CO" orien="R0" />
        <instance x="2432" y="640" name="XLXI_6" orien="R0" />
        <instance x="2432" y="1168" name="XLXI_7" orien="R0" />
        <branch name="XLXN_19">
            <wire x2="2432" y1="640" y2="720" x1="2432" />
            <wire x2="2880" y1="640" y2="640" x1="2432" />
            <wire x2="2880" y1="576" y2="640" x1="2880" />
        </branch>
        <branch name="sum_less(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2048" y="320" type="branch" />
            <wire x2="2048" y1="320" y2="320" x1="1952" />
            <wire x2="2160" y1="320" y2="320" x1="2048" />
        </branch>
        <branch name="sum_great(31:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2080" y="800" type="branch" />
            <wire x2="2080" y1="800" y2="800" x1="1952" />
            <wire x2="2160" y1="800" y2="800" x1="2080" />
        </branch>
        <branch name="sum_less(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2352" y="320" type="branch" />
            <wire x2="2352" y1="320" y2="320" x1="2272" />
            <wire x2="2432" y1="320" y2="320" x1="2352" />
        </branch>
        <branch name="KP_T(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2336" y="448" type="branch" />
            <wire x2="2336" y1="448" y2="448" x1="2272" />
            <wire x2="2432" y1="448" y2="448" x1="2336" />
        </branch>
        <branch name="t(31:16)">
            <wire x2="2320" y1="1360" y2="1360" x1="2192" />
            <wire x2="2432" y1="976" y2="976" x1="2320" />
            <wire x2="2320" y1="976" y2="1360" x1="2320" />
        </branch>
        <branch name="KP_T(15:0)">
            <wire x2="784" y1="1024" y2="1024" x1="480" />
        </branch>
        <iomarker fontsize="28" x="480" y="1024" name="KP_T(15:0)" orien="R180" />
        <instance x="2048" y="1328" name="XLXI_8" orien="R0">
        </instance>
        <branch name="sum_great(31:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2336" y="848" type="branch" />
            <wire x2="2336" y1="848" y2="848" x1="2288" />
            <wire x2="2432" y1="848" y2="848" x1="2336" />
        </branch>
        <branch name="KP_Less_R(15:0)">
            <wire x2="3200" y1="384" y2="384" x1="2880" />
        </branch>
        <branch name="KP_Great_R(15:0)">
            <wire x2="3200" y1="912" y2="912" x1="2880" />
        </branch>
        <branch name="XLXN_35">
            <wire x2="2432" y1="576" y2="576" x1="2400" />
            <wire x2="2400" y1="576" y2="1104" x1="2400" />
            <wire x2="2432" y1="1104" y2="1104" x1="2400" />
            <wire x2="2400" y1="1104" y2="1184" x1="2400" />
            <wire x2="2400" y1="1184" y2="1312" x1="2400" />
        </branch>
        <branch name="KP_CO_Res">
            <wire x2="2960" y1="1104" y2="1104" x1="2880" />
        </branch>
        <iomarker fontsize="28" x="2960" y="1104" name="KP_CO_Res" orien="R0" />
        <iomarker fontsize="28" x="3200" y="384" name="KP_Less_R(15:0)" orien="R0" />
        <iomarker fontsize="28" x="3200" y="912" name="KP_Great_R(15:0)" orien="R0" />
        <instance x="2368" y="1456" name="XLXI_10" orien="R270">
        </instance>
    </sheet>
</drawing>