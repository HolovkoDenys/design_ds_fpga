--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   22:41:14 12/11/2017
-- Design Name:   
-- Module Name:   D:/XI_LABS/KP_lab4/test_tree.vhd
-- Project Name:  KP_lab4
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: KP_tree_pipe_mult
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_tree IS
END test_tree;
 
ARCHITECTURE behavior OF test_tree IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT KP_tree_pipe_mult
    PORT(
         KP_res : IN  std_logic;
         KP_clk : IN  std_logic;
         KP_A : IN  std_logic_vector(15 downto 0);
         KP_B : IN  std_logic_vector(15 downto 0);
         KP_C : OUT  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal KP_res : std_logic := '1';
   signal KP_clk : std_logic := '0';
   signal KP_A : std_logic_vector(15 downto 0) :=x"1234";
   signal KP_B : std_logic_vector(15 downto 0) :=x"3456";

 	--Outputs
   signal KP_C : std_logic_vector(31 downto 0);

   -- Clock period definitions
   
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: KP_tree_pipe_mult PORT MAP (
          KP_res => KP_res,
          KP_clk => KP_clk,
          KP_A => KP_A,
          KP_B => KP_B,
          KP_C => KP_C
        );
	KP_clk <= not KP_clk after 10 ns;
  

   -- Stimulus process
   stim_proc: process
   begin		
      KP_res <= not KP_res after 100 ns;
      wait for 1000 ns;
		KP_A <= x"45EF" after 10 ns;
		KP_B <= x"90C1" after 10 ns;
		wait for 10 ns;
    end process;

END;
