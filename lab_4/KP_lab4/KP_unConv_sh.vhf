--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : KP_unConv_sh.vhf
-- /___/   /\     Timestamp : 12/13/2017 12:16:44
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family artix7 -flat -suppress -vhdl D:/XI_LABS/KP_lab4/KP_unConv_sh.vhf -w D:/XI_LABS/KP_lab4/KP_unConv_sh.sch
--Design Name: KP_unConv_sh
--Device: artix7
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--
----- CELL ADD16_HXILINX_KP_unConv_sh -----
  
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity ADD16_HXILINX_KP_unConv_sh is
port(
       CO  : out std_logic;
       OFL : out std_logic;
       S   : out std_logic_vector(15 downto 0);
    
       A   : in std_logic_vector(15 downto 0);
       B   : in std_logic_vector(15 downto 0);
       CI  : in std_logic
    );
end ADD16_HXILINX_KP_unConv_sh;

architecture ADD16_HXILINX_KP_unConv_sh_V of ADD16_HXILINX_KP_unConv_sh is
  signal adder_tmp: std_logic_vector(16 downto 0);
begin
  adder_tmp <= conv_std_logic_vector((conv_integer(A) + conv_integer(B) + conv_integer(CI)),17);
  S         <= adder_tmp(15 downto 0);
  CO        <= adder_tmp(16);
  OFL <=  ( A(15) and B(15) and (not adder_tmp(15)) ) or ( (not A(15)) and (not B(15)) and adder_tmp(15) );  
          
end ADD16_HXILINX_KP_unConv_sh_V;
 
----- CELL ADSU16_HXILINX_KP_unConv_sh -----
  
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity ADSU16_HXILINX_KP_unConv_sh is
port(
    CO   : out std_logic;
    OFL  : out std_logic;
    S    : out std_logic_vector(15 downto 0);

    A    : in std_logic_vector(15 downto 0);
    ADD  : in std_logic;
    B    : in std_logic_vector(15 downto 0);
    CI   : in std_logic
  );
end ADSU16_HXILINX_KP_unConv_sh;

architecture ADSU16_HXILINX_KP_unConv_sh_V of ADSU16_HXILINX_KP_unConv_sh is

begin
  adsu_p : process (A, ADD, B, CI)
    variable adsu_tmp : std_logic_vector(16 downto 0);
  begin
    if(ADD = '1') then
     adsu_tmp := conv_std_logic_vector((conv_integer(A) + conv_integer(B) + conv_integer(CI)),17);
    else
     adsu_tmp := conv_std_logic_vector((conv_integer(A) - conv_integer(not CI) - conv_integer(B)),17);
  end if;
      
  S   <= adsu_tmp(15 downto 0);
   
  if (ADD='1') then
    CO <= adsu_tmp(16);
    OFL <=  ( A(15) and B(15) and (not adsu_tmp(15)) ) or ( (not A(15)) and (not B(15)) and adsu_tmp(15) );  
  else
    CO <= not adsu_tmp(16);
    OFL <=  ( A(15) and (not B(15)) and (not adsu_tmp(15)) ) or ( (not A(15)) and B(15) and adsu_tmp(15) );  
  end if;
 
  end process;
  
end ADSU16_HXILINX_KP_unConv_sh_V;

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity KP_unConv_sh is
   port ( KP_A       : in    std_logic_vector (15 downto 0); 
          KP_B       : in    std_logic_vector (15 downto 0); 
          KP_D       : in    std_logic_vector (15 downto 0); 
          KP_T       : in    std_logic_vector (15 downto 0); 
          KP_CO      : out   std_logic; 
          KP_CO_Res  : out   std_logic; 
          KP_Great_R : out   std_logic_vector (15 downto 0); 
          KP_Less_R  : out   std_logic_vector (15 downto 0));
end KP_unConv_sh;

architecture BEHAVIORAL of KP_unConv_sh is
   attribute HU_SET     : string ;
   signal ab                   : std_logic_vector (31 downto 0);
   signal d2                   : std_logic_vector (31 downto 0);
   signal sum_great            : std_logic_vector (31 downto 16);
   signal sum_less             : std_logic_vector (15 downto 0);
   signal t                    : std_logic_vector (31 downto 16);
   signal XLXN_2               : std_logic;
   signal XLXN_19              : std_logic;
   signal XLXN_35              : std_logic;
   signal XLXI_4_CI_openSignal : std_logic;
   signal XLXI_6_CI_openSignal : std_logic;
   component kp_mult
      port ( kp_a : in    std_logic_vector (15 downto 0); 
             kp_b : in    std_logic_vector (15 downto 0); 
             kp_c : out   std_logic_vector (31 downto 0));
   end component;
   
   component ADD16_HXILINX_KP_unConv_sh
      port ( A   : in    std_logic_vector (15 downto 0); 
             B   : in    std_logic_vector (15 downto 0); 
             CI  : in    std_logic; 
             CO  : out   std_logic; 
             OFL : out   std_logic; 
             S   : out   std_logic_vector (15 downto 0));
   end component;
   
   component ADSU16_HXILINX_KP_unConv_sh
      port ( A   : in    std_logic_vector (15 downto 0); 
             ADD : in    std_logic; 
             B   : in    std_logic_vector (15 downto 0); 
             CI  : in    std_logic; 
             CO  : out   std_logic; 
             OFL : out   std_logic; 
             S   : out   std_logic_vector (15 downto 0));
   end component;
   
   attribute HU_SET of XLXI_4 : label is "XLXI_4_0";
   attribute HU_SET of XLXI_5 : label is "XLXI_5_1";
   attribute HU_SET of XLXI_6 : label is "XLXI_6_2";
   attribute HU_SET of XLXI_7 : label is "XLXI_7_3";
begin
   t(31 downto 16) <= x"0000";
   XLXN_35 <= '0';
   XLXI_1 : kp_mult
      port map (kp_a(15 downto 0)=>KP_A(15 downto 0),
                kp_b(15 downto 0)=>KP_B(15 downto 0),
                kp_c(31 downto 0)=>ab(31 downto 0));
   
   XLXI_2 : kp_mult
      port map (kp_a(15 downto 0)=>KP_D(15 downto 0),
                kp_b(15 downto 0)=>KP_D(15 downto 0),
                kp_c(31 downto 0)=>d2(31 downto 0));
   
   XLXI_4 : ADD16_HXILINX_KP_unConv_sh
      port map (A(15 downto 0)=>ab(15 downto 0),
                B(15 downto 0)=>d2(15 downto 0),
                CI=>XLXI_4_CI_openSignal,
                CO=>XLXN_2,
                OFL=>open,
                S(15 downto 0)=>sum_less(15 downto 0));
   
   XLXI_5 : ADD16_HXILINX_KP_unConv_sh
      port map (A(15 downto 0)=>ab(31 downto 16),
                B(15 downto 0)=>d2(31 downto 16),
                CI=>XLXN_2,
                CO=>KP_CO,
                OFL=>open,
                S(15 downto 0)=>sum_great(31 downto 16));
   
   XLXI_6 : ADSU16_HXILINX_KP_unConv_sh
      port map (A(15 downto 0)=>sum_less(15 downto 0),
                ADD=>XLXN_35,
                B(15 downto 0)=>KP_T(15 downto 0),
                CI=>XLXI_6_CI_openSignal,
                CO=>XLXN_19,
                OFL=>open,
                S(15 downto 0)=>KP_Less_R(15 downto 0));
   
   XLXI_7 : ADSU16_HXILINX_KP_unConv_sh
      port map (A(15 downto 0)=>sum_great(31 downto 16),
                ADD=>XLXN_35,
                B(15 downto 0)=>t(31 downto 16),
                CI=>XLXN_19,
                CO=>KP_CO_Res,
                OFL=>open,
                S(15 downto 0)=>KP_Great_R(15 downto 0));
   
end BEHAVIORAL;


