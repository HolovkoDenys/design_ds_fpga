----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:17:00 12/11/2017 
-- Design Name: 
-- Module Name:    KP_tree_pipe_mult - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity KP_tree_pipe_mult is
    Port ( KP_res : in  STD_LOGIC;
           KP_clk : in  STD_LOGIC;
           KP_A : in  STD_LOGIC_VECTOR (15 downto 0);
           KP_B : in  STD_LOGIC_VECTOR (15 downto 0);
           KP_C : out  STD_LOGIC_VECTOR (31 downto 0));
end KP_tree_pipe_mult;

architecture Behavioral of KP_tree_pipe_mult is
	constant width: integer := 16;
	signal kp_bv0, kp_bv1, kp_bv2, kp_bv3, kp_bv4, kp_bv5, kp_bv6, kp_bv7, kp_bv8,
kp_bv9, kp_bv10, kp_bv11, kp_bv12, kp_bv13, kp_bv14, kp_bv15: std_logic_vector(width-1 downto 0);
	signal kp_bp0, kp_bp1, kp_bp2, kp_bp3, kp_bp4, kp_bp5, kp_bp6, kp_bp7, kp_bp8,
kp_bp9, kp_bp10, kp_bp11, kp_bp12, kp_bp13, kp_bp14, kp_bp15: unsigned(2*width-1 downto 0);
	signal kp_pp01_reg, kp_pp23_reg, kp_pp45_reg, kp_pp67_reg, kp_pp89_reg, 
kp_pp1011_reg, kp_pp1213_reg, kp_pp1415_reg, KP_pp0_3_reg, KP_pp4_7_reg, KP_pp8_11_reg,
KP_pp12_15_reg, kp_pp0_7, kp_pp8_15, kp_pp0_15_reg: unsigned(2*width-1 downto 0);
	signal kp_pp01_next, kp_pp23_next, kp_pp45_next, kp_pp67_next, kp_pp89_next,
kp_pp1011_next, kp_pp1213_next, kp_pp1415_next, KP_pp0_3_next, KP_pp4_7_next, KP_pp8_11_next,
kp_pp12_15_next, kp_pp0_7_next, kp_pp8_15_next, kp_pp0_15_next: unsigned(2*width-1 downto 0);
begin
	process(KP_clk, KP_res)
	begin
		if(KP_res = '1') then
			kp_pp01_reg   <= (others => '0');
			kp_pp23_reg   <= (others => '0');
			kp_pp45_reg   <= (others => '0');
			kp_pp67_reg   <= (others => '0');
			kp_pp89_reg   <= (others => '0');
			kp_pp1011_reg <= (others => '0');
			kp_pp1213_reg <= (others => '0');
			kp_pp1415_reg <= (others => '0');
			kp_pp0_3_reg  <= (others => '0');
			kp_pp4_7_reg  <= (others => '0');
			kp_pp8_11_reg <= (others => '0');
			kp_pp12_15_reg<= (others => '0');
			kp_pp0_15_reg <= (others => '0');
		elsif(KP_clk'event and KP_clk = '1') then
			kp_pp01_reg   <= kp_pp01_next;
			kp_pp23_reg   <= kp_pp23_next;
			kp_pp45_reg   <= kp_pp45_next;
			kp_pp67_reg   <= kp_pp67_next;
			kp_pp89_reg   <= kp_pp89_next;
			kp_pp1011_reg <= kp_pp1011_next;
			kp_pp1213_reg <= kp_pp1213_next;
			kp_pp1415_reg <= kp_pp1415_next;
			kp_pp0_3_reg  <= kp_pp0_3_next;
			kp_pp4_7_reg  <= kp_pp4_7_next;
			kp_pp8_11_reg <= kp_pp8_11_next;
			kp_pp12_15_reg<= kp_pp12_15_next;
			kp_pp0_15_reg <= kp_pp0_15_next;
		end if;
	end process;
	--stage 1 -- bit product 
	kp_bv0  <= (others => KP_b(0));
	kp_bp0  <= unsigned("0000000000000000" & (kp_bv0 and KP_a));
	kp_bv1  <= (others => KP_b(1));
	kp_bp1  <= unsigned("000000000000000" & (kp_bv1 and KP_a) & "0");
	kp_bv2  <= (others => KP_b(2));
	kp_bp2  <= unsigned("00000000000000" & (kp_bv2 and KP_a) & "00");
	kp_bv3  <= (others => KP_b(3));
	kp_bp3  <= unsigned("0000000000000" & (kp_bv3 and KP_a) & "000");
	kp_bv4  <= (others => KP_b(4));
	kp_bp4  <= unsigned("000000000000" & (kp_bv4 and KP_a) & "0000");
	kp_bv5  <= (others => KP_b(5));
	kp_bp5  <= unsigned("00000000000" & (kp_bv5 and KP_a) & "00000");
	kp_bv6  <= (others => KP_b(6));
	kp_bp6  <= unsigned("0000000000" & (kp_bv6 and KP_a) & "000000");
	kp_bv7  <= (others => KP_b(7));
	kp_bp7  <= unsigned("000000000" & (kp_bv7 and KP_a) & "0000000");
	kp_bv8  <= (others => KP_b(8));
	kp_bp8  <= unsigned("00000000" & (kp_bv8 and KP_a) & "00000000");
	kp_bv9  <= (others => KP_b(9));
	kp_bp9  <= unsigned("0000000" & (kp_bv9 and KP_a) & "000000000");
	kp_bv10 <= (others => KP_b(10));
	kp_bp10 <= unsigned("000000" & (kp_bv10 and KP_a) & "0000000000");
	kp_bv11 <= (others => KP_b(11));
	kp_bp11 <= unsigned("00000" & (kp_bv11 and KP_a) & "00000000000");
	kp_bv12 <= (others => KP_b(12));
	kp_bp12 <= unsigned("0000" & (kp_bv12 and KP_a) & "000000000000");
	kp_bv13 <= (others => KP_b(13));
	kp_bp13 <= unsigned("000" & (kp_bv13 and KP_a) & "0000000000000");
	kp_bv14 <= (others => KP_b(14));
	kp_bp14 <= unsigned("00" & (kp_bv14 and KP_a) & "00000000000000");
	kp_bv15 <= (others => KP_b(15));
	kp_bp15 <= unsigned("0" & (kp_bv15 and KP_a) & "000000000000000");
	--adder
	kp_pp01_next   <= kp_bp0 + kp_bp1;
	kp_pp23_next 	<= kp_bp2 + kp_bp3;
	kp_pp45_next 	<= kp_bp4 + kp_bp5;
	kp_pp67_next 	<= kp_bp6 + kp_bp7;
	kp_pp89_next 	<= kp_bp8 + kp_bp9;
	kp_pp1011_next <= kp_bp10 + kp_bp11;
	kp_pp1213_next <= kp_bp12 + kp_bp13;
	kp_pp1415_next <= kp_bp14 + kp_bp15;
	-- stage 2
	kp_pp0_3_next	<= kp_pp01_reg + kp_pp23_reg;
	kp_pp4_7_next  <= kp_pp45_reg + kp_pp67_reg;
	kp_pp8_11_next <= kp_pp89_reg + kp_pp1011_reg;
	kp_pp12_15_next<= kp_pp1213_reg + kp_pp1415_reg;
	-- stage 3
	kp_pp0_7_next	<= kp_pp0_3_reg + kp_pp4_7_reg;
	kp_pp8_15_next	<= kp_pp8_11_reg + kp_pp12_15_reg;
	-- stage 4
	kp_pp0_15_next	<= kp_pp0_7_next + kp_pp8_15_next;
	
	KP_C <= std_logic_vector(kp_pp0_15_reg);
end Behavioral;

