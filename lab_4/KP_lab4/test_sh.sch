<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="KP_A(15:0)" />
        <signal name="KP_B(15:0)" />
        <signal name="KP_C(31:0)" />
        <port polarity="Input" name="KP_A(15:0)" />
        <port polarity="Input" name="KP_B(15:0)" />
        <port polarity="Output" name="KP_C(31:0)" />
        <blockdef name="KP_mult">
            <timestamp>2017-12-11T14:0:50</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <block symbolname="KP_mult" name="XLXI_1">
            <blockpin signalname="KP_A(15:0)" name="kp_a(15:0)" />
            <blockpin signalname="KP_B(15:0)" name="kp_b(15:0)" />
            <blockpin signalname="KP_C(31:0)" name="kp_c(31:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="720" y="272" name="XLXI_1" orien="R0">
        </instance>
        <branch name="KP_A(15:0)">
            <wire x2="720" y1="176" y2="176" x1="640" />
        </branch>
        <branch name="KP_B(15:0)">
            <wire x2="704" y1="240" y2="240" x1="640" />
            <wire x2="720" y1="240" y2="240" x1="704" />
        </branch>
        <branch name="KP_C(31:0)">
            <wire x2="1184" y1="176" y2="176" x1="1104" />
        </branch>
        <iomarker fontsize="28" x="640" y="176" name="KP_A(15:0)" orien="R180" />
        <iomarker fontsize="28" x="1184" y="176" name="KP_C(31:0)" orien="R0" />
        <iomarker fontsize="28" x="640" y="240" name="KP_B(15:0)" orien="R180" />
    </sheet>
</drawing>