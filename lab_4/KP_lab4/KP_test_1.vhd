library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
entity gvv_mult is
 Port ( gvv_clk : in STD_LOGIC;
 gvv_reset : in STD_LOGIC;
 gvv_a : in STD_LOGIC_VECTOR (4 downto 0);
 gvv_b : in STD_LOGIC_VECTOR (4 downto 0);
 gvv_y : out STD_LOGIC_VECTOR (9 downto 0));
end gvv_mult;
architecture Behavioral of gvv_mult is
constant WIDTH: integer:=5;
signal gvv_a0, gvv_a1, gvv_a2, gvv_a3 :
std_logic_vector(WIDTH-1 downto 0);
signal gvv_b0, gvv_b1, gvv_b2, gvv_b3 :
std_logic_vector(WIDTH-1 downto 0);
signal gvv_bv0, gvv_bv1, gvv_bv2, gvv_bv3, gvv_bv4 :
std_logic_vector(WIDTH - 1 downto 0);
signal gvv_bp0, gvv_bp1, gvv_bp2, gvv_bp3, gvv_bp4 :
unsigned(2*WIDTH - 1 downto 0);
signal gvv_pp0, gvv_pp1, gvv_pp2, gvv_pp3, gvv_pp4 :
unsigned(2*WIDTH - 1 downto 0);
begin
-- stage 0
gvv_bv0 <= (others => gvv_b(0));
gvv_bp0 <= unsigned("00000" & (gvv_bv0 and gvv_a));
gvv_pp0 <= gvv_bp0;
gvv_a0 <= gvv_a;
gvv_b0 <= gvv_b;
-- stage 1
gvv_bv1 <= (others => gvv_b0(1));
gvv_bp1 <= unsigned("0000" & (gvv_bv1 and gvv_a0) & "0");
gvv_pp1 <= gvv_pp0 + gvv_bp1;
gvv_a1 <= gvv_a0;
gvv_b1 <= gvv_b0;
-- stage 2
gvv_bv2 <= (others => gvv_b1(2));
gvv_bp2 <= unsigned("000" & (gvv_bv2 and gvv_a1) & "00");
gvv_pp2 <= gvv_pp1 + gvv_bp2;
gvv_a2 <= gvv_a1;
gvv_b2 <= gvv_b1;
-- stage 3
gvv_bv3 <= (others => gvv_b2(3));
gvv_bp3 <= unsigned("00" & (gvv_bv3 and gvv_a2) & "000");
gvv_pp3 <= gvv_pp2 + gvv_bp3;
gvv_a3 <= gvv_a2;
gvv_b3 <= gvv_b2;
-- stage 4
gvv_bv4 <= (others => gvv_b3(4));
gvv_bp4 <= unsigned("0" & (gvv_bv4 and gvv_a3) & "0000");
gvv_pp4 <= gvv_pp3 + gvv_bp4;
-- result
gvv_y <= std_logic_vector(gvv_pp4);
end Behavioral;