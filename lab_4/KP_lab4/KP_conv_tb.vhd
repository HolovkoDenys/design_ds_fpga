-- Vhdl test bench created from schematic D:\XI_LABS\KP_lab4\KP_conv_sh.sch - Tue Dec 12 14:55:16 2017
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY KP_conv_sh_KP_conv_sh_sch_tb IS
END KP_conv_sh_KP_conv_sh_sch_tb;
ARCHITECTURE behavioral OF KP_conv_sh_KP_conv_sh_sch_tb IS 

   COMPONENT KP_conv_sh
   PORT( KP_A	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KP_B	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KP_D	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KP_T	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KP_reset	:	IN	STD_LOGIC; 
          KP_clk	:	IN	STD_LOGIC; 
          KP_ce	:	IN	STD_LOGIC; 
          KP_CO_sum	:	OUT	STD_LOGIC; 
          KP_OFL_sum	:	OUT	STD_LOGIC; 
          KP_CO_fin	:	OUT	STD_LOGIC; 
          KP_OFL_fin	:	OUT	STD_LOGIC; 
          KP_res_less	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KP_res_great	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0));
   END COMPONENT;

   SIGNAL KP_A	:	STD_LOGIC_VECTOR (15 DOWNTO 0):=x"0000";
   SIGNAL KP_B	:	STD_LOGIC_VECTOR (15 DOWNTO 0):=x"0000";
   SIGNAL KP_D	:	STD_LOGIC_VECTOR (15 DOWNTO 0):=x"0000";
   SIGNAL KP_T	:	STD_LOGIC_VECTOR (15 DOWNTO 0):=x"0000";
   SIGNAL KP_reset	:	STD_LOGIC:='1';
   SIGNAL KP_clk	:	STD_LOGIC:='0';
   SIGNAL KP_ce	:	STD_LOGIC:='0';
   SIGNAL KP_CO_sum	:	STD_LOGIC;
   SIGNAL KP_OFL_sum	:	STD_LOGIC;
   SIGNAL KP_CO_fin	:	STD_LOGIC;
   SIGNAL KP_OFL_fin	:	STD_LOGIC;
   SIGNAL KP_res_less	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL KP_res_great	:	STD_LOGIC_VECTOR (15 DOWNTO 0);

BEGIN

   UUT: KP_conv_sh PORT MAP(
		KP_A => KP_A, 
		KP_B => KP_B, 
		KP_D => KP_D, 
		KP_T => KP_T, 
		KP_reset => KP_reset, 
		KP_clk => KP_clk, 
		KP_ce => KP_ce, 
		KP_CO_sum => KP_CO_sum, 
		KP_OFL_sum => KP_OFL_sum, 
		KP_CO_fin => KP_CO_fin, 
		KP_OFL_fin => KP_OFL_fin, 
		KP_res_less => KP_res_less, 
		KP_res_great => KP_res_great
   );
	KP_clk <= not KP_clk after 10 ns;
	KP_reset <= '0' after 100 ns;
	KP_ce <= '1' after 150 ns;
-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
		KP_A <= x"0001" after 100 ns;
		KP_B <= x"0002" after 100 ns;
		KP_D <= x"0003" after 100 ns;
		KP_T <= x"0004" after 100 ns;
		wait for 100 ns;
	
		KP_A <= x"0005" after 100 ns;
		KP_B <= x"0006" after 100 ns;
		KP_D <= x"0007" after 100 ns;
		KP_T <= x"0008" after 100 ns;
		WAIT for 100 ns;
       -- will wait forever
		
		KP_A <= x"0009" after 100 ns;
		KP_B <= x"000A" after 100 ns;
		KP_D <= x"000B" after 100 ns;
		KP_T <= x"000C" after 100 ns;
		wait for 100 ns;
		
		KP_A <= x"9345" after 100 ns;
		KP_B <= x"8A4C" after 100 ns;
		KP_D <= x"1F5B" after 100 ns;
		KP_T <= x"74EC" after 100 ns;
		wait for 100 ns;
		
		KP_A <= x"34EC" after 100 ns;
		KP_B <= x"4FE1" after 100 ns;
		KP_D <= x"509E" after 100 ns;
		KP_T <= x"1111" after 100 ns;
		wait for 100 ns;
		
		KP_A <= x"E36C" after 100 ns;
		KP_B <= x"05CD" after 100 ns;
		KP_D <= x"1EC9" after 100 ns;
		KP_T <= x"34CD" after 100 ns;
		wait for 100 ns;
		
		--wait;
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
