<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="KP_A(7:0)" />
        <signal name="A1(14:0)" />
        <signal name="K2(7:0)" />
        <signal name="K3(7:0)" />
        <signal name="XLXN_19(7:0)" />
        <signal name="B3(14:0)" />
        <signal name="B2(14:0)" />
        <signal name="B1(14:0)" />
        <signal name="KP_CE" />
        <signal name="KP_CLK" />
        <signal name="KP_CLR" />
        <signal name="A1B1B2(15:0)" />
        <signal name="A1B1(15:0)" />
        <signal name="B2(15:0)" />
        <signal name="B3(15:0)" />
        <signal name="A1(15:0)" />
        <signal name="B1(15:0)" />
        <signal name="Filter(15:0)" />
        <signal name="KP_Y(15:0)" />
        <signal name="CO1" />
        <signal name="CO2" />
        <signal name="A1(15)" />
        <signal name="B1(15)" />
        <signal name="B2(15)" />
        <signal name="B3(15)" />
        <signal name="XLXN_63" />
        <port polarity="Input" name="KP_A(7:0)" />
        <port polarity="Input" name="KP_CE" />
        <port polarity="Input" name="KP_CLK" />
        <port polarity="Input" name="KP_CLR" />
        <port polarity="Output" name="KP_Y(15:0)" />
        <blockdef name="KP_K_mM_105">
            <timestamp>2017-12-13T13:14:58</timestamp>
            <rect width="304" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-44" height="24" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="KP_K_mM_110">
            <timestamp>2017-12-13T13:15:6</timestamp>
            <rect width="304" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-44" height="24" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="fd8ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="fd16ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <block symbolname="KP_K_mM_105" name="XLXI_1">
            <blockpin signalname="KP_A(7:0)" name="KP_A(7:0)" />
            <blockpin signalname="A1(14:0)" name="KP_C_mM(14:0)" />
        </block>
        <block symbolname="KP_K_mM_105" name="XLXI_2">
            <blockpin signalname="K2(7:0)" name="KP_A(7:0)" />
            <blockpin signalname="B1(14:0)" name="KP_C_mM(14:0)" />
        </block>
        <block symbolname="KP_K_mM_110" name="XLXI_3">
            <blockpin signalname="K3(7:0)" name="KP_A(7:0)" />
            <blockpin signalname="B2(14:0)" name="KP_C_mM(14:0)" />
        </block>
        <block symbolname="KP_K_mM_110" name="XLXI_4">
            <blockpin signalname="XLXN_19(7:0)" name="KP_A(7:0)" />
            <blockpin signalname="B3(14:0)" name="KP_C_mM(14:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_9">
            <blockpin signalname="KP_CLK" name="C" />
            <blockpin signalname="KP_CE" name="CE" />
            <blockpin signalname="KP_CLR" name="CLR" />
            <blockpin signalname="KP_A(7:0)" name="D(7:0)" />
            <blockpin signalname="K2(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_10">
            <blockpin signalname="KP_CLK" name="C" />
            <blockpin signalname="KP_CE" name="CE" />
            <blockpin signalname="KP_CLR" name="CLR" />
            <blockpin signalname="K2(7:0)" name="D(7:0)" />
            <blockpin signalname="K3(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_11">
            <blockpin signalname="KP_CLK" name="C" />
            <blockpin signalname="KP_CE" name="CE" />
            <blockpin signalname="KP_CLR" name="CLR" />
            <blockpin signalname="K3(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_19(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="add16" name="XLXI_14">
            <blockpin signalname="B2(15:0)" name="A(15:0)" />
            <blockpin signalname="A1B1(15:0)" name="B(15:0)" />
            <blockpin signalname="CO1" name="CI" />
            <blockpin signalname="CO2" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="A1B1B2(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_13">
            <blockpin signalname="A1(15:0)" name="A(15:0)" />
            <blockpin signalname="B1(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_63" name="CI" />
            <blockpin signalname="CO1" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="A1B1(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_15">
            <blockpin signalname="B3(15:0)" name="A(15:0)" />
            <blockpin signalname="A1B1B2(15:0)" name="B(15:0)" />
            <blockpin signalname="CO2" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="Filter(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_17">
            <blockpin signalname="KP_CLK" name="C" />
            <blockpin signalname="KP_CE" name="CE" />
            <blockpin signalname="KP_CLR" name="CLR" />
            <blockpin signalname="Filter(15:0)" name="D(15:0)" />
            <blockpin signalname="KP_Y(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_18">
            <blockpin signalname="A1(15)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_19">
            <blockpin signalname="B1(15)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_20">
            <blockpin signalname="B2(15)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_21">
            <blockpin signalname="B3(15)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_30">
            <blockpin signalname="XLXN_63" name="G" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="784" y="1168" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1472" y="1040" name="XLXI_2" orien="R0">
        </instance>
        <instance x="3008" y="1040" name="XLXI_4" orien="R0">
        </instance>
        <instance x="672" y="672" name="XLXI_9" orien="R0" />
        <instance x="1504" y="672" name="XLXI_10" orien="R0" />
        <branch name="KP_A(7:0)">
            <wire x2="512" y1="416" y2="416" x1="384" />
            <wire x2="672" y1="416" y2="416" x1="512" />
            <wire x2="512" y1="416" y2="1136" x1="512" />
            <wire x2="784" y1="1136" y2="1136" x1="512" />
        </branch>
        <iomarker fontsize="28" x="384" y="416" name="KP_A(7:0)" orien="R180" />
        <branch name="A1(14:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="1136" type="branch" />
            <wire x2="1248" y1="1136" y2="1136" x1="1216" />
            <wire x2="1296" y1="1136" y2="1136" x1="1248" />
        </branch>
        <branch name="K2(7:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="416" type="branch" />
            <wire x2="1184" y1="416" y2="416" x1="1056" />
            <wire x2="1184" y1="416" y2="1008" x1="1184" />
            <wire x2="1472" y1="1008" y2="1008" x1="1184" />
            <wire x2="1248" y1="416" y2="416" x1="1184" />
            <wire x2="1504" y1="416" y2="416" x1="1248" />
        </branch>
        <instance x="2224" y="672" name="XLXI_11" orien="R0" />
        <branch name="K3(7:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2080" y="416" type="branch" />
            <wire x2="2080" y1="416" y2="416" x1="1888" />
            <wire x2="2224" y1="416" y2="416" x1="2080" />
            <wire x2="2080" y1="416" y2="1008" x1="2080" />
            <wire x2="2224" y1="1008" y2="1008" x1="2080" />
        </branch>
        <instance x="2224" y="1040" name="XLXI_3" orien="R0">
        </instance>
        <branch name="XLXN_19(7:0)">
            <wire x2="2800" y1="416" y2="416" x1="2608" />
            <wire x2="2800" y1="416" y2="1008" x1="2800" />
            <wire x2="3008" y1="1008" y2="1008" x1="2800" />
        </branch>
        <branch name="B3(14:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3488" y="1008" type="branch" />
            <wire x2="3488" y1="1008" y2="1008" x1="3440" />
            <wire x2="3536" y1="1008" y2="1008" x1="3488" />
        </branch>
        <branch name="B2(14:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2688" y="1008" type="branch" />
            <wire x2="2688" y1="1008" y2="1008" x1="2656" />
            <wire x2="2752" y1="1008" y2="1008" x1="2688" />
        </branch>
        <branch name="B1(14:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1952" y="1008" type="branch" />
            <wire x2="1952" y1="1008" y2="1008" x1="1904" />
            <wire x2="2016" y1="1008" y2="1008" x1="1952" />
        </branch>
        <branch name="KP_CE">
            <wire x2="672" y1="480" y2="480" x1="384" />
        </branch>
        <branch name="KP_CLK">
            <wire x2="400" y1="544" y2="544" x1="384" />
            <wire x2="672" y1="544" y2="544" x1="400" />
        </branch>
        <branch name="KP_CLR">
            <wire x2="672" y1="640" y2="640" x1="384" />
        </branch>
        <iomarker fontsize="28" x="384" y="480" name="KP_CE" orien="R180" />
        <iomarker fontsize="28" x="384" y="544" name="KP_CLK" orien="R180" />
        <iomarker fontsize="28" x="384" y="640" name="KP_CLR" orien="R180" />
        <branch name="KP_CE">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1456" y="480" type="branch" />
            <wire x2="1456" y1="480" y2="480" x1="1408" />
            <wire x2="1504" y1="480" y2="480" x1="1456" />
        </branch>
        <branch name="KP_CLK">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1456" y="544" type="branch" />
            <wire x2="1456" y1="544" y2="544" x1="1408" />
            <wire x2="1504" y1="544" y2="544" x1="1456" />
        </branch>
        <branch name="KP_CLR">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1472" y="640" type="branch" />
            <wire x2="1424" y1="640" y2="640" x1="1408" />
            <wire x2="1472" y1="640" y2="640" x1="1424" />
            <wire x2="1504" y1="640" y2="640" x1="1472" />
        </branch>
        <branch name="KP_CE">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2176" y="480" type="branch" />
            <wire x2="2176" y1="480" y2="480" x1="2128" />
            <wire x2="2224" y1="480" y2="480" x1="2176" />
        </branch>
        <branch name="KP_CLK">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2160" y="544" type="branch" />
            <wire x2="2160" y1="544" y2="544" x1="2128" />
            <wire x2="2224" y1="544" y2="544" x1="2160" />
        </branch>
        <branch name="KP_CLR">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2192" y="640" type="branch" />
            <wire x2="2192" y1="640" y2="640" x1="2128" />
            <wire x2="2224" y1="640" y2="640" x1="2192" />
        </branch>
        <instance x="1776" y="1760" name="XLXI_14" orien="R0" />
        <instance x="1024" y="1760" name="XLXI_13" orien="R0" />
        <instance x="2496" y="1760" name="XLXI_15" orien="R0" />
        <branch name="A1B1B2(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2368" y="1568" type="branch" />
            <wire x2="2352" y1="1504" y2="1504" x1="2224" />
            <wire x2="2352" y1="1504" y2="1568" x1="2352" />
            <wire x2="2368" y1="1568" y2="1568" x1="2352" />
            <wire x2="2496" y1="1568" y2="1568" x1="2368" />
        </branch>
        <branch name="A1B1(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1536" y="1504" type="branch" />
            <wire x2="1536" y1="1504" y2="1504" x1="1472" />
            <wire x2="1616" y1="1504" y2="1504" x1="1536" />
            <wire x2="1616" y1="1504" y2="1568" x1="1616" />
            <wire x2="1776" y1="1568" y2="1568" x1="1616" />
        </branch>
        <branch name="B2(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1680" y="1440" type="branch" />
            <wire x2="1680" y1="1440" y2="1440" x1="1616" />
            <wire x2="1776" y1="1440" y2="1440" x1="1680" />
        </branch>
        <branch name="B3(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2400" y="1440" type="branch" />
            <wire x2="2400" y1="1440" y2="1440" x1="2368" />
            <wire x2="2496" y1="1440" y2="1440" x1="2400" />
        </branch>
        <branch name="A1(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="912" y="1440" type="branch" />
            <wire x2="912" y1="1440" y2="1440" x1="864" />
            <wire x2="1024" y1="1440" y2="1440" x1="912" />
        </branch>
        <branch name="B1(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="912" y="1568" type="branch" />
            <wire x2="912" y1="1568" y2="1568" x1="864" />
            <wire x2="1024" y1="1568" y2="1568" x1="912" />
        </branch>
        <branch name="Filter(15:0)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3008" y="1616" type="branch" />
            <wire x2="3008" y1="1504" y2="1504" x1="2944" />
            <wire x2="3008" y1="1504" y2="1616" x1="3008" />
            <wire x2="3008" y1="1616" y2="1808" x1="3008" />
            <wire x2="3120" y1="1808" y2="1808" x1="3008" />
        </branch>
        <branch name="KP_Y(15:0)">
            <wire x2="3584" y1="1808" y2="1808" x1="3504" />
        </branch>
        <branch name="CO1">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1488" y="1696" type="branch" />
            <wire x2="1488" y1="1696" y2="1696" x1="1472" />
            <wire x2="1600" y1="1696" y2="1696" x1="1488" />
            <wire x2="1600" y1="1312" y2="1696" x1="1600" />
            <wire x2="1776" y1="1312" y2="1312" x1="1600" />
        </branch>
        <branch name="CO2">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2384" y="1312" type="branch" />
            <wire x2="2336" y1="1696" y2="1696" x1="2224" />
            <wire x2="2336" y1="1312" y2="1696" x1="2336" />
            <wire x2="2384" y1="1312" y2="1312" x1="2336" />
            <wire x2="2496" y1="1312" y2="1312" x1="2384" />
        </branch>
        <instance x="3120" y="2064" name="XLXI_17" orien="R0" />
        <branch name="KP_CE">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3088" y="1872" type="branch" />
            <wire x2="3088" y1="1872" y2="1872" x1="3008" />
            <wire x2="3120" y1="1872" y2="1872" x1="3088" />
        </branch>
        <branch name="KP_CLK">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3056" y="1936" type="branch" />
            <wire x2="3056" y1="1936" y2="1936" x1="3008" />
            <wire x2="3120" y1="1936" y2="1936" x1="3056" />
        </branch>
        <branch name="KP_CLR">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3072" y="2032" type="branch" />
            <wire x2="3072" y1="2032" y2="2032" x1="3008" />
            <wire x2="3120" y1="2032" y2="2032" x1="3072" />
        </branch>
        <instance x="288" y="1920" name="XLXI_18" orien="R0" />
        <instance x="560" y="1920" name="XLXI_19" orien="R0" />
        <instance x="832" y="1920" name="XLXI_20" orien="R0" />
        <instance x="1136" y="1920" name="XLXI_21" orien="R0" />
        <branch name="A1(15)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="400" y="1728" type="branch" />
            <wire x2="352" y1="1728" y2="1792" x1="352" />
            <wire x2="400" y1="1728" y2="1728" x1="352" />
            <wire x2="496" y1="1728" y2="1728" x1="400" />
        </branch>
        <branch name="B1(15)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="656" y="1744" type="branch" />
            <wire x2="624" y1="1744" y2="1792" x1="624" />
            <wire x2="656" y1="1744" y2="1744" x1="624" />
            <wire x2="736" y1="1744" y2="1744" x1="656" />
        </branch>
        <branch name="B2(15)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="912" y="1744" type="branch" />
            <wire x2="896" y1="1744" y2="1792" x1="896" />
            <wire x2="912" y1="1744" y2="1744" x1="896" />
            <wire x2="1008" y1="1744" y2="1744" x1="912" />
        </branch>
        <branch name="B3(15)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1216" y="1744" type="branch" />
            <wire x2="1200" y1="1744" y2="1792" x1="1200" />
            <wire x2="1216" y1="1744" y2="1744" x1="1200" />
            <wire x2="1280" y1="1744" y2="1744" x1="1216" />
        </branch>
        <iomarker fontsize="28" x="3584" y="1808" name="KP_Y(15:0)" orien="R0" />
        <instance x="816" y="1248" name="XLXI_30" orien="R90" />
        <branch name="XLXN_63">
            <wire x2="1024" y1="1312" y2="1312" x1="944" />
        </branch>
    </sheet>
</drawing>